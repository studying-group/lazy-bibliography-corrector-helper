from before import before_string
from after import after_string


def get_key_dict(string: str) -> dict:
    splitted_by_n = string.split('\n')[1:-1]
    key_value = {}
    for item in splitted_by_n:
        number, source = item.split('\t')
        source: str = source.strip()
        key_value.update({source: number})

    return key_value


def get_changes_source_number_list(dict_1: dict, dict_2: dict) -> list:
    changes_list = []
    for key, value in dict_2.items():
        dict_1_value = dict_1.get(key)
        if value == dict_1_value: continue

        changes_list.append("{0} -> {1}".format(
            dict_1_value,
            value
        ))

    return changes_list


if __name__ == '__main__':
    before_key_dict = get_key_dict(before_string)
    after_key_dict = get_key_dict(after_string)

    result = get_changes_source_number_list(before_key_dict, after_key_dict)
    for item in result:
        print(item)