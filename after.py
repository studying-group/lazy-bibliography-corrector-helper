after_string = """
1)	Касты / Энциклопедия Кругосвет - [Электронный ресурс] - режим доступа: https://www.krugosvet.ru/enc/religiya/kasty (дата обращения: 19.06.2021)
2)	Цензура / Толковый словарь Ожегова и Шведовой - [Электронный ресурс] - режим доступа: https://gufo.me/dict/ozhegov/цензура (дата обращения: 19.06.2021)
3)	Горяева Т. М. Политическая цензура в СССР. 1917-1991. - 2. - М.: «Российская политическая энциклопедия» (РОССПЭН), 2009. - С. 8-9. - 407 с. - (История сталинизма). - 2000 экз
4)	Постановление о присоединении СССР к Факультативному протоколу к Международному пакту о гражданских и политических правах от 5 июля 1991 г. № 2304-I
5)	Хайек, Фридрих Август фон Дорога к рабству / Пер. с англ. М.: Новое издательство, 2005. — 264 с. (Библиотека Фонда «Либеральная миссия»)
6)	Выбор интеллектуала: эмиграция или самоцензура? / Мнения / Republic - [Электронный ресурс] - режим доступа: https://republic.ru/posts/l/948691 (дата обращения: 20.06.2021)
7)	Цензура / Электронная еврейская энциклопедия ОРТ - [Электронный ресурс] - режим доступа: https://eleven.co.il/diaspora/power-society-and-jews/14609/ (дата обращения 20.06.2021)
8)	Censorship / Britannica - [Электронный ресурс] - режим доступа: https://www.britannica.com/topic/censorship (дата обращения 20.06.2021)
9)	Поппер К. Примечания к главе 4. Глава 6. Тоталитаристская справедливость // Открытое общество и его враги = The Open Society and Its Enemies. — Феникс, 1992. — Т. 1. — 447 с.
10)	Green J., Karolides N.J. Encyclopedia of Censorship // Library of Congress Cataloging-in-Publication Data, 1948 — 697 p.
11)	Левченко И. Е. Проблема цензуры в истории зарубежной общественно-политической мысли // Социально-политический журнал. — М., 1996. — Вып. 6. — С. 179—192.
12)	А. С. Пушкин. Собрание сочинений в 10 томах. — М.: ГИХЛ, 1959—1962. — Том 1. Стихотворения 1814—1822. — С. 195—198.
13)	Жирков Г. В. История цензуры в России XIX—XX века. — Аспект-Пресс, 2001. — 368 с.
14)	Сидорова М. В. Библиотеки нелегальных изданий при учреждениях политического сыска // Леонов В. П., Савельева Е. А. Книга в России : сборник научных трудов. — Библиотека Российской академии наук, 2004. — Т. 21, вып. 21.
15)	Сидорова М. В. Библиотеки нелегальных изданий при учреждениях политического сыска // Леонов В. П., Савельева Е. А. Книга в России : сборник научных трудов. — Библиотека Российской академии наук, 2004. — Т. 21, вып. 21.
16)	Nazi Propaganda and Censorship. the United States Holocaust Memorial Museum - [Электронный ресурс] - режим доступа: https://encyclopedia.ushmm.org/content/en/article/nazi-propaganda-and-censorship (дата обращения 20.06.2021)
17)	Плейкис Римантас. Радиоцензура // «КАРТА» : Российский независимый исторический и правозащитный журнал. — 2005. — № 43—44. — С. 117—136.
18)	Кульгин М. Технологии корпоративных сетей. Энциклопедия. — СПб.: Питер, 2000. — 509 с.
19)	Алексеев И. Цензурный инфантилизм. Призывы ввести цензуру – попытка Церкви переложить свою работу на "власть" или признак отчаяния и паники? - [Электронный ресурс] - режим доступа: http://www.portal-credo.ru/site/print.php?act=comment&id=1180 (дата обращения 20.06.2021)
20)	Указ Президента РФ от 29 мая 2020 г. № 344 “Об утверждении Стратегии противодействия экстремизму в Российской Федерации до 2025 года”
21)	«Их возможности безграничны»Социальные сети имеют огромную власть над всем миром. Чем это опасно? / LENTA.RU - [Электронный ресурс] - режим доступа:  https://lenta.ru/articles/2020/11/27/social_networks/ (дата обращения 20.06.2021)
22)	Eli Pariser. The Filter Bubble: What the Internet Is Hiding from You. — New York: Penguin Press, 2011
23)	Пузырь фильтров (filter bubble), а также 10 шагов, как вырваться из плена своих интересов - [Электронный ресурс] - режим доступа: https://habr.com/ru/post/132191/ (дата обращения 20.06.2021)
24)	Eli Pariser. Beware online “filter bubbles” - [Электронный ресурс] - режим доступа:  https://www.ted.com/talks/eli_pariser_beware_online_filter_bubbles/transcript#t-9465 (дата обращения 20.06.2021)
25)	Your personal data is nobody's business. - [Электронный ресурс] - режим доступа: https://duckduckgo.com/about (дата обращения 20.06.2021)
26)	Никита Данюк: Культурно-информационные войны. Часть четвертая - [Электронный ресурс] - режим доступа: https://antimaidan.ru/article/12598 (дата обращения 20.06.2021)
27)	Eli J. Finkel, Christopher A. Bail, Mina Cikara, Peter H. Ditto, Shanto Iyengar, Samara Klar, Lilliana Mason, Mary C. McGrath1, Brendan Nyhan, David G. Rand, Linda J. Skitka, Joshua A. Tucker, Jay J. Van Bavel, Cynthia S. Wang, James N. Druckman. Political sectarianism in America // Science  30 Oct 2020: Vol. 370, Issue 6516, pp. 533-536
28)	This Scary Statistic Predicts Growing US Political Violence — Whatever Happens On Election Day - [Электронный ресурс] - режим доступа: https://www.buzzfeednews.com/article/peteraldhous/political-violence-inequality-us-election (дата обращения 20.06.2021)
29)	Клинтон потратила на предвыборную кампанию в два раза больше Трампа - [Электронный ресурс] - режим доступа: https://www.rbc.ru/politics/07/11/2016/58172d119a79478828b87ccc
30)	Ellen Nakashima. U.S. government officially accuses Russia of hacking campaign to interfere with elections. The Washington Post (7 October 2016) - [Электронный ресурс] - режим доступа: https://www.washingtonpost.com/world/national-security/us-government-officially-accuses-russia-of-hacking-campaign-to-influence-elections/2016/10/07/4e0b9654-8cbf-11e6-875e-2c1bfe943b66_story.html?noredirect=on (дата обращения 20.06.2021)
31)	Блокировка Трампа в соцсетях. Что пишут немецкие СМИ. Deutsche Welle (11.01.2021) - [Электронный ресурс] - режим доступа: https://www.dw.com/ru/blokirovka-trampa-v-socsetjah-chto-pishut-nemeckie-smi/a-56194577 (дата обращения 20.06.2021)
32)	Stephan Lewandowsky, Michael Jetter & Ullrich K. H. Ecker. Using the president’s tweets to understand political diversion in the age of social media. // Nature Communications volume 11, Article number: 5764 (2020).
33)	Президентский твит: зачем политики заигрывают с соцсетями - [Электронный ресурс] - режим доступа: https://www.forbes.ru/tehnologii/362247-prezidentskiy-tvit-zachem-politiki-zaigryvayut-s-socsetyami (дата обращения 20.06.2021)
34)	Интернет проиграл эмоциям: почему технологии Обамы не спасли Клинтон - [Электронный ресурс] - режим доступа: https://www.eurointegration.com.ua/rus/articles/2016/11/11/7057272/ (дата обращения 20.06.2021)
35)	Твит Трампа обвалил капитализацию Amazon на $5,7 млрд - [Электронный ресурс] - режим доступа: https://biz.liga.net/all/it/novosti/tvit-trampa-obvalil-kapitalizatsiyu-amazon-na-5-7-mlrd (дата обращения 20.06.2021)
36)	Трамп одним твитом спровоцировал рост цен на нефть - [Электронный ресурс] - режим доступа: https://www.bbc.com/russian/news-52141551 (дата обращения 20.06.2021)
37)	Суд запретил Трампу блокировать критиков в Twitter - [Электронный ресурс] - режим доступа: https://www.forbes.ru/obshchestvo/379693-sud-zapretil-trampu-blokirovat-kritikov-v-twitter (дата обращения 21.06.2021)
38)	COVFEFE Act would preserve Trump's tweets as official statements - [Электронный ресурс] - режим доступа: https://www.reuters.com/article/us-usa-trump-covfefe-idUSKBN19402C (дата обращения 21.06.2021)
39)	Элемент информационной войны: как Twitter-дипломатия Трампа влияет на международную политику - [Электронный ресурс] - режим доступа: https://russian.rt.com/world/article/686383-ssha-tramp-pompeo-twitter-diplomatiya (дата обращения 21.06.2021)
40)	"Я - современный президент": почему Трамп не откажется от соцсетей - [Электронный ресурс] - режим доступа: https://www.bbc.com/russian/features-40472700 (дата обращения 21.06.2021)
41)	«Это его оружие»: как Трамп ведет войны в твиттере - [Электронный ресурс] - режим доступа: https://www.gazeta.ru/politics/2018/07/08_a_11830435.shtml (дата обращения 21.06.2021)
42)	How hate and misinformation go viral: A case study of a Trump retweet - [Электронный ресурс] - режим доступа: https://www.brookings.edu/techstream/how-hate-and-misinformation-go-viral-a-case-study-of-a-trump-retweet/ (дата обращения 21.06.2021)
43)	Дональд Трамп сделал запись в "Твиттере". Почему это раскололо Facebook - [Электронный ресурс] - режим доступа: https://www.bbc.com/russian/news-52908944 (дата обращения 21.06.2021)
44)	Donald Trump says Black Lives Matter is becoming a 'symbol of hate' in furious Twitter tirade  - [Электронный ресурс] - режим доступа: https://www.sbs.com.au/news/donald-trump-says-black-lives-matter-is-becoming-a-symbol-of-hate-in-furious-twitter-tirade (дата обращения 21.06.2021)
45)	Твит Трампа о том, что грипп опаснее коронавируса, пометили как дезинформирующий - [Электронный ресурс] - режим доступа: https://tass.ru/obschestvo/9644275/amp (дата обращения 21.06.2021)
46)	"Попытка мятежа": сторонники Трампа ворвались в Капитолий - [Электронный ресурс] - режим доступа: https://ria.ru/20210107/vashington-1592195862.html (дата обращения 21.06.2021)
47)	Twitter навсегда заблокировал аккаунт Дональда Трампа  - [Электронный ресурс] - режим доступа: https://www.kommersant.ru/doc/4639344?from=hotnews#id1999325 (дата обращения 21.06.2021)
48)	Капитализация Facebook и Twitter снизилась на $44 млрд после блокировки Трампа - [Электронный ресурс] - режим доступа: https://www.kommersant.ru/doc/4640646 (дата обращения 21.06.2021)
49)	Angela Merkel betrachtet Twitter-Sperrung als problematisch - [Электронный ресурс] - режим доступа: https://www.stuttgarter-nachrichten.de/inhalt.twitter-konto-von-donald-trump-angela-merkel-betrachtet-twitter-sperrung-als-problematisch.c3ffee1b-cb71-49ca-9a4e-599b91b69c74.html (дата обращения 21.06.2021)
50)	Эрдоган назвал блокировки Трампа в соцсетях «цифровым фашизмом» - [Электронный ресурс] - режим доступа:  https://news.ru/world/erdogan-nazval-blokirovki-trampa-cifrovym-fashizmom/ (дата обращения 21.06.2021)
51)	«Просто взяли и отключили»: Захарова назвала блокировку аккаунтов Трампа ударом по декларируемым Западом ценностям  - [Электронный ресурс] - режим доступа: https://russian.rt.com/world/article/822093-tramp-socseti-blokirovka (дата обращения 21.06.2021)
52)	Скачайте архив своих данных  - [Электронный ресурс] - режим доступа: https://twitter.com/settings/download_your_data (дата обращения 21.06.2021)
53)	Защита данных ВКонтакте - [Электронный ресурс] - режим доступа: https://vk.com/data_protection?section=rules (дата обращения 21.06.2021)
54)	Как экспортировать данные статистики моей Страницы Facebook? - [Электронный ресурс] - режим доступа: https://badge.facebook.com/help/972879969525875 (дата обращения 21.06.2021)
55)	Скачивание копии вашей информации (Instagram) - [Электронный ресурс] - режим доступа: https://www.instagram.com/download/request/  (дата обращения 21.06.2021)
56)	Как сохранить историю чатов (WhatsApp) - [Электронный ресурс] - режим доступа: https://faq.whatsapp.com/android/chats/how-to-save-your-chat-history/?lang=ru (дата обращения 21.06.2021)
57)	Как создать резервную копию чатов в Viber - [Электронный ресурс] - режим доступа: https://help.viber.com/ru/article/как-создать-резервную-копию-чатов-в-viber (дата обращения 21.06.2021)
58)	Chat Export Tool, Better Notifications and More (Telegram) - [Электронный ресурс] - режим доступа: https://telegram.org/blog/export-and-more (дата обращения 21.06.2021)
59)	15 сервисов для автопостинга в социальных сетях - [Электронный ресурс] - режим доступа: https://netology.ru/blog/autopostingservices (дата обращения 21.06.2021)
"""