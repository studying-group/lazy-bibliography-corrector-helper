before_string = """
1)	Касты / Энциклопедия Кругосвет - [Электронный ресурс] - режим доступа: https://www.krugosvet.ru/enc/religiya/kasty (дата обращения: 19.06.2021)
2)	Цензура / Толковый словарь Ожегова и Шведовой - [Электронный ресурс] - режим доступа: https://gufo.me/dict/ozhegov/цензура (дата обращения: 19.06.2021)
3)	Горяева Т. М. Политическая цензура в СССР. 1917-1991. - 2. - М.: «Российская политическая энциклопедия» (РОССПЭН), 2009. - С. 8-9. - 407 с. - (История сталинизма). - 2000 экз
4)	United Nations Treaty Series, vol. 999, p. 225–240.
5)	Постановление о присоединении СССР к Факультативному протоколу к Международному пакту о гражданских и политических правах от 5 июля 1991 г. № 2304-I
6)	Хайек, Фридрих Август фон Дорога к рабству / Пер. с англ. М.: Новое издательство, 2005. — 264 с. (Библиотека Фонда «Либеральная миссия»)
7)	Закон РФ от 21.07.1993 № 5485-1 (ред. от 08.03.2015) «О государственной тайне».  - [Электронный ресурс] - режим доступа: http://www.consultant.ru/. (дата обращения: 20.06.2021)
8)	Константинов А. И., Иванов Д. В. Военная цензура как условие победы. // Военно-исторический журнал. — 2000. — № 4. — С.20-27.
9)	Яхнина Ю. С. Вехи мировой цензуры: к 450-летию со дня издания первого свода папского «Индекса запрещённых книг» / Челябинская областная универсальная научная библиотека.
10)	Myron Fox. Censorship! War Letters. WGBH American Experience - [Электронный ресурс] - режим доступа: https://www.pbs.org/wgbh/americanexperience/features/warletters-censorship/ (дата обращения: 20.06.2021)
11)	Иванов Д. Нагрудный знак военных цензоров. 1916 (рус.) // Военная иллюстрация : военно-исторический альманах. — М., 1998. — № 1. — С. 48.
12)	Timothy Jay (2000). Why We Curse: A Neuro-psycho-social Theory of Speech. John Benjamins Publishing Company. pp. 208—209.
13)	Выбор интеллектуала: эмиграция или самоцензура? / Мнения / Republic - [Электронный ресурс] - режим доступа: https://republic.ru/posts/l/948691 (дата обращения: 20.06.2021)
14)	Цензура / Электронная еврейская энциклопедия ОРТ - [Электронный ресурс] - режим доступа: https://eleven.co.il/diaspora/power-society-and-jews/14609/ (дата обращения 20.06.2021)
15)	Censorship / Britannica - [Электронный ресурс] - режим доступа: https://www.britannica.com/topic/censorship (дата обращения 20.06.2021)
16)	Поппер К. Примечания к главе 4. Глава 6. Тоталитаристская справедливость // Открытое общество и его враги = The Open Society and Its Enemies. — Феникс, 1992. — Т. 1. — 447 с.
17)	Green J., Karolides N.J. Encyclopedia of Censorship // Library of Congress Cataloging-in-Publication Data, 1948 — 697 p.
18)	Яхнина Ю. С. Вехи мировой цензуры: к 450-летию со дня издания первого свода папского «Индекса запрещённых книг» // Челябинская областная универсальная научная библиотека.
19)	Николюкин А.Н. Литературная энциклопедия терминов и понятий. 2001. // НПК «Интелвак». М.: 799 с.
20)	Левченко И. Е. Проблема цензуры в истории зарубежной общественно-политической мысли // Социально-политический журнал. — М., 1996. — Вып. 6. — С. 179—192.
21)	А. С. Пушкин. Собрание сочинений в 10 томах. — М.: ГИХЛ, 1959—1962. — Том 1. Стихотворения 1814—1822. — С. 195—198.
22)	Жирков Г. В. История цензуры в России XIX—XX века. — Аспект-Пресс, 2001. — 368 с.
23)	Сидорова М. В. Библиотеки нелегальных изданий при учреждениях политического сыска // Леонов В. П., Савельева Е. А. Книга в России : сборник научных трудов. — Библиотека Российской академии наук, 2004. — Т. 21, вып. 21.
24)	Сидорова М. В. Библиотеки нелегальных изданий при учреждениях политического сыска // Леонов В. П., Савельева Е. А. Книга в России : сборник научных трудов. — Библиотека Российской академии наук, 2004. — Т. 21, вып. 21.
25)	Nazi Propaganda and Censorship. the United States Holocaust Memorial Museum - [Электронный ресурс] - режим доступа: https://encyclopedia.ushmm.org/content/en/article/nazi-propaganda-and-censorship (дата обращения 20.06.2021)
26)	Плейкис Римантас. Радиоцензура // «КАРТА» : Российский независимый исторический и правозащитный журнал. — 2005. — № 43—44. — С. 117—136.
27)	Кульгин М. Технологии корпоративных сетей. Энциклопедия. — СПб.: Питер, 2000. — 509 с.
28)	Аджемов А. С. Международная информационная безопасность: проблемы и решения. — М.: Наука, 2011. — С. 25-28.
29)	Philipp Winter, Stefan Lindskog. How China Is Blocking Tor. — Karlstad: Karlstad University, 2012. — 21 p.
30)	Блюм А. В. Оруэлл на Лубянке - [Электронный ресурс] - режим доступа: https://newtimes.ru/articles/detail/3642/ (дата обращения 20.06.2021)
31)	Творчество Дж. Оруэлла. Антиутопия «1984» - [Электронный ресурс] - режим доступа: http://www.gumfak.ru/zarub_html/bilet3/zaruba21.shtml (дата обращения 20.06.2021)
32)	Приговор для всех // Огонёк : журнал. — 1998. — 28 июня (№ 25 (4560)). — С. 3.
33)	Алексеев И. Цензурный инфантилизм. Призывы ввести цензуру – попытка Церкви переложить свою работу на "власть" или признак отчаяния и паники? - [Электронный ресурс] - режим доступа: http://www.portal-credo.ru/site/print.php?act=comment&id=1180 (дата обращения 20.06.2021)
34)	Указ Президента РФ от 29 мая 2020 г. № 344 “Об утверждении Стратегии противодействия экстремизму в Российской Федерации до 2025 года”
35)	«Их возможности безграничны»Социальные сети имеют огромную власть над всем миром. Чем это опасно? / LENTA.RU - [Электронный ресурс] - режим доступа:  https://lenta.ru/articles/2020/11/27/social_networks/ (дата обращения 20.06.2021)
36)	Eli Pariser. The Filter Bubble: What the Internet Is Hiding from You. — New York: Penguin Press, 2011
37)	Пузырь фильтров (filter bubble), а также 10 шагов, как вырваться из плена своих интересов - [Электронный ресурс] - режим доступа: https://habr.com/ru/post/132191/ (дата обращения 20.06.2021)
38)	Eli Pariser. Beware online “filter bubbles” - [Электронный ресурс] - режим доступа:  https://www.ted.com/talks/eli_pariser_beware_online_filter_bubbles/transcript#t-9465 (дата обращения 20.06.2021)
39)	First Monday: What's on tap this month on TV and in movies and books: The Filter Bubble by Eli Pariser, USA Today (2011) - [Электронный ресурс] - режим доступа: https://usatoday30.usatoday.com/money/companies/management/monday/2011-04-29-first-monday-business-media-in-may_n.htm#mainstory (дата обращения 20.06.2021)
40)	Jacob Weisberg. Bubble Trouble: Is Web personalization turning us into solipsistic twits?, Slate Magazine - [Электронный ресурс] - режим доступа:  https://slate.com/news-and-politics/2011/06/eli-pariser-s-the-filter-bubble-is-web-personalization-turning-us-into-solipsistic-twits.html (дата обращения 20.06.2021)
41)	Facebook отрицает «пузырь фильтров» и призывает СМИ отдавать ему статьи / theRunet - [Электронный ресурс] - режим доступа: https://therunet.com/news/4055-facebook-otritsaet-puzyr-filtrov-i-prizyvaet-smi-otdavat-emu-stati
42)	Your personal data is nobody's business. - [Электронный ресурс] - режим доступа: https://duckduckgo.com/about (дата обращения 20.06.2021)
43)	Никита Данюк: Культурно-информационные войны. Часть четвертая - [Электронный ресурс] - режим доступа: https://antimaidan.ru/article/12598 (дата обращения 20.06.2021)
44)	Eli J. Finkel, Christopher A. Bail, Mina Cikara, Peter H. Ditto, Shanto Iyengar, Samara Klar, Lilliana Mason, Mary C. McGrath1, Brendan Nyhan, David G. Rand, Linda J. Skitka, Joshua A. Tucker, Jay J. Van Bavel, Cynthia S. Wang, James N. Druckman. Political sectarianism in America // Science  30 Oct 2020: Vol. 370, Issue 6516, pp. 533-536
45)	This Scary Statistic Predicts Growing US Political Violence — Whatever Happens On Election Day - [Электронный ресурс] - режим доступа: https://www.buzzfeednews.com/article/peteraldhous/political-violence-inequality-us-election (дата обращения 20.06.2021)
46)	Клинтон потратила на предвыборную кампанию в два раза больше Трампа - [Электронный ресурс] - режим доступа: https://www.rbc.ru/politics/07/11/2016/58172d119a79478828b87ccc
47)	Andrew Bosworth (Boz) - [Электронный ресурс] - режим доступа:  https://www.facebook.com/boz/posts/10111288357877121 (дата обращения 20.06.2021)
48)	Ellen Nakashima. U.S. government officially accuses Russia of hacking campaign to interfere with elections. The Washington Post (7 October 2016) - [Электронный ресурс] - режим доступа: https://www.washingtonpost.com/world/national-security/us-government-officially-accuses-russia-of-hacking-campaign-to-influence-elections/2016/10/07/4e0b9654-8cbf-11e6-875e-2c1bfe943b66_story.html?noredirect=on (дата обращения 20.06.2021)
49)	Evan Perez and Daniella Diaz. White House announces retaliation against Russia: Sanctions, ejecting diplomats. CNN (3 January 2017) - [Электронный ресурс] - режим доступа: https://edition.cnn.com/2016/12/29/politics/russia-sanctions-announced-by-white-house/index.html (дата обращения 20.06.2021)
50)	Блокировка Трампа в соцсетях. Что пишут немецкие СМИ. Deutsche Welle (11.01.2021) - [Электронный ресурс] - режим доступа: https://www.dw.com/ru/blokirovka-trampa-v-socsetjah-chto-pishut-nemeckie-smi/a-56194577 (дата обращения 20.06.2021)
51)	Stephan Lewandowsky, Michael Jetter & Ullrich K. H. Ecker. Using the president’s tweets to understand political diversion in the age of social media. // Nature Communications volume 11, Article number: 5764 (2020).
52)	Президентский твит: зачем политики заигрывают с соцсетями - [Электронный ресурс] - режим доступа: https://www.forbes.ru/tehnologii/362247-prezidentskiy-tvit-zachem-politiki-zaigryvayut-s-socsetyami (дата обращения 20.06.2021)
53)	Интернет проиграл эмоциям: почему технологии Обамы не спасли Клинтон - [Электронный ресурс] - режим доступа: https://www.eurointegration.com.ua/rus/articles/2016/11/11/7057272/ (дата обращения 20.06.2021)
54)	Твит Трампа обвалил капитализацию Amazon на $5,7 млрд - [Электронный ресурс] - режим доступа: https://biz.liga.net/all/it/novosti/tvit-trampa-obvalil-kapitalizatsiyu-amazon-na-5-7-mlrd (дата обращения 20.06.2021)
55)	Трамп одним твитом спровоцировал рост цен на нефть - [Электронный ресурс] - режим доступа: https://www.bbc.com/russian/news-52141551 (дата обращения 20.06.2021)
56)	Twitter изменит правила, чтобы не удалять твиты Дональда Трампа - [Электронный ресурс] - режим доступа: https://ednews.net/ru/news/world/193057-twitter-izmenit-pravila-tchtobi-ne-udalyat-tviti-donalda-trampa (дата обращения 20.06.2021)
57)	Суд запретил Трампу блокировать критиков в Twitter - [Электронный ресурс] - режим доступа: https://www.forbes.ru/obshchestvo/379693-sud-zapretil-trampu-blokirovat-kritikov-v-twitter (дата обращения 21.06.2021)
58)	COVFEFE Act would preserve Trump's tweets as official statements - [Электронный ресурс] - режим доступа: https://www.reuters.com/article/us-usa-trump-covfefe-idUSKBN19402C (дата обращения 21.06.2021)
59)	Элемент информационной войны: как Twitter-дипломатия Трампа влияет на международную политику - [Электронный ресурс] - режим доступа: https://russian.rt.com/world/article/686383-ssha-tramp-pompeo-twitter-diplomatiya (дата обращения 21.06.2021)
60)	"Я - современный президент": почему Трамп не откажется от соцсетей - [Электронный ресурс] - режим доступа: https://www.bbc.com/russian/features-40472700 (дата обращения 21.06.2021)
61)	«Это его оружие»: как Трамп ведет войны в твиттере - [Электронный ресурс] - режим доступа: https://www.gazeta.ru/politics/2018/07/08_a_11830435.shtml (дата обращения 21.06.2021)
62)	Трамп анонсировал депортацию миллионов мигрантов - [Электронный ресурс] - режим доступа: https://www.gazeta.ru/politics/news/2019/06/18/n_13104649.shtml (дата обращения 21.06.2021)
63)	How hate and misinformation go viral: A case study of a Trump retweet - [Электронный ресурс] - режим доступа: https://www.brookings.edu/techstream/how-hate-and-misinformation-go-viral-a-case-study-of-a-trump-retweet/ (дата обращения 21.06.2021)
64)	Дональд Трамп сделал запись в "Твиттере". Почему это раскололо Facebook - [Электронный ресурс] - режим доступа: https://www.bbc.com/russian/news-52908944 (дата обращения 21.06.2021)
65)	Donald Trump says Black Lives Matter is becoming a 'symbol of hate' in furious Twitter tirade  - [Электронный ресурс] - режим доступа: https://www.sbs.com.au/news/donald-trump-says-black-lives-matter-is-becoming-a-symbol-of-hate-in-furious-twitter-tirade (дата обращения 21.06.2021)
66)	Твит Трампа о том, что грипп опаснее коронавируса, пометили как дезинформирующий - [Электронный ресурс] - режим доступа: https://tass.ru/obschestvo/9644275/amp (дата обращения 21.06.2021)
67)	"Попытка мятежа": сторонники Трампа ворвались в Капитолий - [Электронный ресурс] - режим доступа: https://ria.ru/20210107/vashington-1592195862.html (дата обращения 21.06.2021)
68)	Противостояние Дональда Трампа и соцсетей. Как президент США лишился своих аккаунтов - [Электронный ресурс] - режим доступа: https://tass.ru/info/10437049 (дата обращения 21.06.2021)
69)	Twitter навсегда заблокировал аккаунт Дональда Трампа  - [Электронный ресурс] - режим доступа: https://www.kommersant.ru/doc/4639344?from=hotnews#id1999325 (дата обращения 21.06.2021)
70)	Капитализация Facebook и Twitter снизилась на $44 млрд после блокировки Трампа - [Электронный ресурс] - режим доступа: https://www.kommersant.ru/doc/4640646 (дата обращения 21.06.2021)
71)	Angela Merkel betrachtet Twitter-Sperrung als problematisch - [Электронный ресурс] - режим доступа: https://www.stuttgarter-nachrichten.de/inhalt.twitter-konto-von-donald-trump-angela-merkel-betrachtet-twitter-sperrung-als-problematisch.c3ffee1b-cb71-49ca-9a4e-599b91b69c74.html (дата обращения 21.06.2021)
72)	Эрдоган назвал блокировки Трампа в соцсетях «цифровым фашизмом» - [Электронный ресурс] - режим доступа:  https://news.ru/world/erdogan-nazval-blokirovki-trampa-cifrovym-fashizmom/ (дата обращения 21.06.2021)
73)	«Просто взяли и отключили»: Захарова назвала блокировку аккаунтов Трампа ударом по декларируемым Западом ценностям  - [Электронный ресурс] - режим доступа: https://russian.rt.com/world/article/822093-tramp-socseti-blokirovka (дата обращения 21.06.2021)
74)	Социальные сети и цензура: за и против / ВЦИОМ - [Электронный ресурс] - режим доступа: https://wciom.ru/analytical-reviews/analiticheskii-obzor/socialnye-seti-i-cenzura-za-i-protiv (дата обращения 21.06.2021)
75)	Федеральный закон "О внесении изменений в Федеральный закон "О связи" и Федеральный закон "Об информации, информационных технологиях и о защите информации" от 01.05.2019 N 90-ФЗ (последняя редакция)
76)	Клименко: Россия должна быть готова к отключению от мирового интернета - [Электронный ресурс] - режим доступа: https://tass.ru/obschestvo/3914882 (дата обращения 21.06.2021)
77)	Подписан закон об административной ответственности за неисполнение закона «об устойчивом Рунете», за цензуру в соцсетях и за нарушения обработки ПД. - [Электронный ресурс] - режим доступа: https://d-russia.ru/podpisan-zakon-ob-administrativnoj-otvetstvennosti-za-neispolnenie-zakona-ob-ustojchivom-runete-za-cenzuru-v-socsetjah-i-za-narushenija-obrabotki-pd.html (дата обращения 21.06.2021)
78)	Более 40% россиян считают цензурой блокировку Трампа в соцсетях - [Электронный ресурс] - режим доступа: https://news.rambler.ru/conflicts/45742028-bolee-40-rossiyan-schitayut-tsenzuroy-blokirovku-trampa-v-sotssetyah/ (дата обращения 21.06.2021)
79)	Скачайте архив своих данных  - [Электронный ресурс] - режим доступа: https://twitter.com/settings/download_your_data (дата обращения 21.06.2021)
80)	Защита данных ВКонтакте - [Электронный ресурс] - режим доступа: https://vk.com/data_protection?section=rules (дата обращения 21.06.2021)
81)	Как экспортировать данные статистики моей Страницы Facebook? - [Электронный ресурс] - режим доступа: https://badge.facebook.com/help/972879969525875 (дата обращения 21.06.2021)
82)	Скачивание копии вашей информации (Instagram) - [Электронный ресурс] - режим доступа: https://www.instagram.com/download/request/  (дата обращения 21.06.2021)
83)	Как сохранить историю чатов (WhatsApp) - [Электронный ресурс] - режим доступа: https://faq.whatsapp.com/android/chats/how-to-save-your-chat-history/?lang=ru (дата обращения 21.06.2021)
84)	Как создать резервную копию чатов в Viber - [Электронный ресурс] - режим доступа: https://help.viber.com/ru/article/как-создать-резервную-копию-чатов-в-viber (дата обращения 21.06.2021)
85)	Chat Export Tool, Better Notifications and More (Telegram) - [Электронный ресурс] - режим доступа: https://telegram.org/blog/export-and-more (дата обращения 21.06.2021)
86)	15 сервисов для автопостинга в социальных сетях - [Электронный ресурс] - режим доступа: https://netology.ru/blog/autopostingservices (дата обращения 21.06.2021)
"""